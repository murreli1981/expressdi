const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const compression = require("compression");

const routes = require("./routes");

const startServer = () => {
  const app = express();
  app.use(cors());
  app.use(helmet());
  app.use(compression());
  app.use(express.json());
  app.use(routes());

  return app;
};

module.exports = startServer;
