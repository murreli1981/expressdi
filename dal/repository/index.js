const data = require("../../mock/data.json");
const userRepository = require("./userRepository");

module.exports = {
  userRepository: userRepository(data),
};
