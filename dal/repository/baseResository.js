const { userDto } = require("../Dto/usersDto");

const baseRepository = (data) => ({
  getAll() {
    return data.map((user) => userDto(user));
  },

  getById(userId) {
    console.log(data);
    const val = data.find(({ _id }) => _id === userId);
    console.log(val);
    return val;
  },

  create(newUser) {
    const lastItem = data[data.length - 1].id;
    const userToCreate = { ...newUser, id: lastItem + 1 };
    data.push(userToCreate);
    return userToCreate;
  },

  update(userId, updateData) {
    const el = data.map(({ data }) => {
      if (data.id === userId) {
        data = { ...data, updateData };
      }
      return data;
    });
    return "User Updated";
  },

  delete(userId) {
    return data.filter(({ id }) => id !== userId);
  },
});

module.exports = baseRepository;
