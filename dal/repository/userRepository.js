const baseRepository = require("./baseResository");

const userRepository = (data) => ({
  ...baseRepository(data),
  getTopElements: (limit) => {
    return data.splice(0, limit);
  },
});

module.exports = userRepository;
