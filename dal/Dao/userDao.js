const data = require("../../mock/data.json");

exports.getAll = () => {
  return data; // metodo para obtener todos los elementos
  // si usas SQL acá va la query a la DB
  // si usas Mongo va el ODM para obtener lso elementos
};

exports.getById = (userId) => {
  return data.find(({ id }) => id === userId);
};

exports.create = (newUser) => {
  const lastItem = data[data.length - 1].id;
  const userToCreate = { ...newUser, id: lastItem + 1 };
  data.push(userToCreate);
  return userToCreate;
};

exports.update = (userId, updateData) => {
  const el = data.map(({ data }) => {
    if (data.id === userId) {
      data = { ...data, updateData };
    }
    return data;
  });
  return "User Updated";
};

exports.delete = (userId) => {
  return data.filter(({ id }) => id !== userId);
};
