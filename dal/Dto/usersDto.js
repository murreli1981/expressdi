exports.userDto = (obj) => {
  const { _id, _name, ...data } = obj;
  return { id: _id, name: _name, ...data };
};
