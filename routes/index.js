const router = require("express").Router();
const { userController } = require("../controllers");

module.exports = routerConfig = () => {
  router.get("/users", userController.getAllUsers);
  router.get("/users/:id", userController.getUserById);
  router.get("/users/limit/:limit", userController.getUsersByLimit);

  return router;
};
