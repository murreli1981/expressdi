const server = require("./server");

server().listen(9000, (err) => {
  if (err) throw new Error(err);
  console.log("Server on port: 9000");
});
