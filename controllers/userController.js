const userController = (service) => {
  const { userService } = service;
  return {
    getAllUsers: (req, res, next) => {
      const users = userService.getAllUsers();
      res.json({ users });
    },

    getUserById: (req, res, next) => {
      const { id } = req.params;
      const user = userService.getById(id);
      res.json(user);
    },
    getUsersByLimit: (req, res, next) => {
      const { limit } = req.params;
      const users = userService.getUsersByLimit(limit);
      res.json(users);
    },
  };
};

module.exports = userController;
