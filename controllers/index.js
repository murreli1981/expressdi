const service = require("../services");
const userController = require("./userController");

module.exports = {
  userController: userController(service),
};
