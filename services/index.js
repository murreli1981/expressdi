const dao = require("../dal/Dao");
const repositories = require("../dal/repository");
const userService = require("./userService");

module.exports = {
  userService: userService(repositories),
};
