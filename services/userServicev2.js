module.exports = class UserService {
  getAllUsers() {
    const list = [
      { id: 1, user: "test" },
      { id: 2, user: "test" },
      { id: 3, user: "test" },
      { id: 4, user: "test" },
    ];
    return list.map((data) => ({ userid: data.id, userName: data.user }));
  }
};
