const userService = ({ userRepository }) => ({
  getAllUsers() {
    return userRepository.getAll();
  },

  getById(id) {
    console.log(id);
    return userRepository.getById(Number(id));
  },

  getUsersByLimit(limit) {
    const users = userRepository.getTopElements(limit);
    return users;
  },
});

module.exports = userService;
